import 'package:flutter/material.dart';
import 'package:tugas/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter layout demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Koperasi UNDIKSHA'),
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Image.asset(
                'images/Logo_undiksha.png',
                width: 300,
              ),
            ),
            // SizedBox(
            //   height: 20,
            // ),
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Container(
                padding: EdgeInsets.all(16.0),
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 50),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromARGB(255, 0, 0, 0)),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(children: [
                  TextField(
                    decoration: InputDecoration(
                      labelText: 'Username',
                    ),
                  ),
                  SizedBox(height: 16.0), // Spasi antara input
                  TextField(
                    obscureText: true, // Menyembunyikan teks password
                    decoration: InputDecoration(
                      labelText: 'Password',
                    ),
                  ),
                  SizedBox(height: 16.0), // Spasi antara input
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Home())); // Tombol aksi (misalnya, untuk login) dapat ditambahkan di sini.
                    },
                    child: Text('Login'),
                  ),
                  SizedBox(
                      height: 16.0), // Spasi antara tombol dan teks tambahan
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () {
                          // Aksi ketika tombol "Daftar" ditekan
                        },
                        child: Text('Daftar Mbanking'),
                      ),
                      TextButton(
                        onPressed: () {
                          // Aksi ketika tombol "Lupa Password" ditekan
                        },
                        child: Text('Lupa Password?'),
                      ),
                    ],
                  ),
                ]),
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 50.0, // Tinggi app bar bawah
            color: Colors.blue, // Warna app bar bawah
            child: Center(
              child: Text(
                'Copyright @2022 by Undiksha',
                style: TextStyle(
                  color: Colors.white, // Warna teks di app bar bawah
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
