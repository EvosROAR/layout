import 'package:flutter/material.dart';
import 'package:tugas/main.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Koperasi UNDIKSHA'),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MyApp()));
                },
                icon: Icon(Icons.logout_outlined)),
          ],
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                border: Border.all(color: const Color.fromARGB(255, 0, 0, 0)),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Row(
                children: [
                  Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: Image.asset(
                        'images/WhatsApp Image 2023-09-13 at 21.48.37.jpg',
                        width: 75,
                        height: 75,
                      ),
                    ),
                  ),
                  Column(children: [
                    Container(
                        width: 210,
                        height: 20,
                        margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: const Color.fromARGB(255, 0, 0, 0)),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Text(
                          'Nasabah : Nuno Tamada',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          bottom: 17), // Jarak horizontal antara dua container
                      width: 210,
                      height: 20,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromARGB(255, 0, 0, 0)),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Text(
                        'Saldo : 1.200.000',
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ]),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MenuIcon(
                  icon: Icons.account_balance_wallet,
                  label: 'Cek Saldo',
                ),
                MenuIcon(
                  icon: Icons.send,
                  label: 'Transfer',
                ),
                MenuIcon(
                  icon: Icons.account_balance,
                  label: 'Deposito',
                ),
              ],
            ),
            SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MenuIcon(
                  icon: Icons.credit_card,
                  label: 'Peminjaman',
                ),
                MenuIcon(
                  icon: Icons.compare_arrows,
                  label: 'Mutasi',
                ),
                MenuIcon(
                  icon: Icons.payment,
                  label: 'Pembayaran',
                ),
              ],
            ),
            SizedBox(
                height:
                    32.0), // Jarak yang lebih besar antara baris-baris menu dan kalimat "Hubungi Sekarang"
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(width: 8.0), // Spasi antara ikon dan teks
                Text(
                  'Hubungi Sekarang',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 8.0), // Spasi antara kalimat dan nomor handphone
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.phone,
                  color: Colors.green, // Warna ikon telepon
                  size: 24.0, // Ukuran ikon telepon
                ),
                SizedBox(width: 8.0), // Spasi antara ikon dan nomor handphone
                Text(
                  'Nomor Handphone: 0890198273',
                  style: TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ],
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: Row // Spasi antara teks nomor handphone dan ikon bawah
              (mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            // Ikon Setting (kiri)
            Icon(
              Icons.settings,
              size: 32.0, // Ukuran ikon setting
            ),
            // Ikon QR Code (tengah)
            Icon(
              Icons.qr_code,
              size: 32.0, // Ukuran ikon QR code
            ),
            // Ikon Profile (kanan)
            Icon(
              Icons.person,
              size: 32.0, // Ukuran ikon profile
            ),
          ]),
        ),
      ),
    );
  }
}

class MenuIcon extends StatelessWidget {
  final IconData icon;
  final String label;

  MenuIcon({required this.icon, required this.label});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 60.0,
          height: 60.0,
          decoration: BoxDecoration(
            color: Colors.blue,
            shape: BoxShape.circle,
          ),
          child: Icon(
            icon,
            color: Colors.white,
            size: 30.0,
          ),
        ),
        SizedBox(height: 8.0),
        Text(label),
      ],
    );
  }
}
